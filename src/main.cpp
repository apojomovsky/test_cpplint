/*
 * Copyright (C) 2016 Ekumen Inc.
*/

#include <iostream>

using namespace std;

int main() {
  cout << "Hello World!" << endl;
  return 0;
}
