## General repository configurations

Make sure you have installed cppcheck by running:
``
sudo apt-get update && sudo apt-get install cppcheck
```

Please add the pre-commit hook before commiting:

```
ln tools/pre-commit.sh .git/hooks/pre-commit
```
